import os
import json
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

# Set up Selenium Chrome driver
chrome_options = Options()
chrome_options.add_argument("--headless")  # Run Chrome in headless mode
service = Service('path/to/chromedriver')  # Replace with the actual path to your chromedriver executable
driver = webdriver.Chrome(service=service, options=chrome_options)

# Define the sections to scrape
sections = ['/business', '/technology']

# Create a directory to store the downloaded articles
output_dir = 'articles'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)
    print('-------------------------',os.path)

# Load the list of already downloaded article titles

downloaded_articles = set()
downloaded_file = os.path.join(output_dir, 'downloaded_articles.txt')
if os.path.exists(downloaded_file):
    with open(downloaded_file, 'r') as f:
        downloaded_articles = set(f.read().splitlines())

# Process each section
for section in sections:
    # Load the section page
    url = 'https://www.bbc.com/news' + section
    driver.get(url)
    driver.implicitly_wait(5)  # Wait for page content to load

    # Extract article links from the section page
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    articles = soup.find_all('a', class_='gs-c-promo-heading')
    for article in articles:
        article_url = article['href']
        article_title = article.text.strip()

        # Check if article has already been downloaded
        if article_title in downloaded_articles:
            print(f'Skipping already downloaded article: {article_title}')
            continue

        # Load the article page
        driver.get(article_url)
        driver.implicitly_wait(5)  # Wait for page content to load

        # Extract the article content
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        article_body = soup.find('div', class_='ssrcss-uf6wea-RichTextContainer e1xue1i64').get_text()

        # Save the article content to a JSON file
        article_data = {
            'title': article_title,
            'body': article_body
        }
        article_filename = article_title.replace(' ', '_') + '.json'
        article_filepath = os.path.join(output_dir, article_filename)
        with open(article_filepath, 'w') as f:
            json.dump(article_data, f)

        # Add the article title to the downloaded set
        downloaded_articles.add(article_title)
        print(f'Downloaded article: {article_title}')

# Save the updated list of downloaded article titles
with open(downloaded_file, 'w') as f:
    f.write('\n'.join(downloaded_articles))

# Quit the driver
driver.quit()
