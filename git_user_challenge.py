import requests
import json
import os



api_key = 'IXgFPJZ5XTNE12Vcg2ju'
# github username = keoopx
# sub-domain freshdesk = 'dallas-help'


def get_github_user(username):
    url = "https://api.github.com/users/{}".format(username)
    print(url)
    response = requests.get(url, headers={"Authorization": "token {}".format(os.environ["GITHUB_TOKEN"])})
    print(response.status_code)
    if response.status_code == 200:
        return json.loads(response.content)
    else:
        return None

def create_or_update_freshdesk_contact(github_user, subdomain):
    
    url = f'https://{subdomain}.freshdesk.com/api/v2/contacts'
    
    # Request headers
    headers = {
        'Content-Type': 'application/json',
    }
    
    # Request payload
    contact_data = {
        
        "name": github_user["name"],
        "email": github_user["email"],
        "twitter_id": github_user["twitter_username"],
        "phone": 3106355503,
        "job_title": github_user["organizations_url"],                
    }
    
    existing_contact = get_contact_by_email(github_user["email"])
    
    
    if existing_contact:
        # Update the existing contact
        contact_id = existing_contact['id']
        update_url = f'{url}/{contact_id}'
        response = requests.put(update_url, auth=(api_key, 'X'), headers=headers, data=json.dumps(contact_data))
        
        if response.status_code == 200:
            print('Contact updated successfully!')
        else:
            print('Failed to update contact. Status code:', response.status_code)
            print('Error message:', response.text)
    else:
    
        # Make a POST request to create the contact
        response = requests.post(url, auth=(api_key, 'X'), headers=headers, data=json.dumps(contact_data))
        
        # Check the response status code
        if response.status_code == 201:
            print('Contact created successfully!')
        else:
            print('Failed to create contact. Status code:', response.status_code)
            print('Error message:', response.text)
    
        

def get_contact_by_email(email):
    url = f'https://{subdomain}.freshdesk.com/api/v2/contacts'
    
    # Request headers
    headers = {
        'Content-Type': 'application/json',
    }
    
    # Request parameters
    params = {
        'email': email,
    }
    
    # Make a GET request to search for the contact
    response = requests.get(url, auth=(api_key, 'X'), headers=headers, params=params)
    
    if response.status_code == 200:
        contacts = response.json()
        if len(contacts) > 0:
            # Return the first matching contact
            return contacts[0]
    else:
        print('Failed to search for contact. Status code:', response.status_code)
        print('Error message:', response.text)
    
    return None

    
        

    
if __name__ == "__main__":
    username = input("Enter GitHub username: ")
    subdomain = input("Enter Freshdesk subdomain: ")
    #print('dsadasdasda')
    github_user = get_github_user(username)
    print(github_user)
    if github_user:
        create_or_update_freshdesk_contact(github_user, subdomain)
