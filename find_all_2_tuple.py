import pytest

def find_similar_tuples(similar_ids):
    result = set()

    for key, values in similar_ids.items():
        for value in values:
            if (value, key) not in result:
                result.add((key, value))

    expected = sorted(result)
    return expected

def test_find_similar_tuples():
    similar_ids = { 
        123: [458, 812, 765], 
        458: [123, 812, 765], 
        812: [123, 458], 
        765: [123, 458], 
        999: [100], 
        100: [999] 
    }

    expected_result = [(100, 999), (123, 458), (123, 765), (123, 812), (458, 765), (458, 812)]

    assert find_similar_tuples(similar_ids) == expected_result

if __name__ == '__main__':
    pytest.main()
